#pragma once
#include <iostream>
#include <vector>
#include "gcode_handling.h"
#include <string>

//#define WINDOWS
#ifdef WINDOWS
	#define	_PRINTF sprintf_s
	#define _STRCPY strcpy_s
	#define _STRCAT strcat_s
#else	
	/**/
	#include <cstring>
	#include <stdio.h>
	/**/
	#define	_PRINTF sprintf
	#define _STRCPY strcpy
	#define _STRCAT strcat

#endif

using namespace std;
#define LENGTH_PER_HZ (double)0.00125 // 3200Hz --> 4mm
class hcode
{
	long int stepX, stepY, stepZ;
	int action;
	long ID;
public:

	void hc_setstepX(long int step);
	void hc_setstepY(long int step);
	void hc_setstepZ(long int step);

	long int hc_getstepX();
	long int hc_getstepY();
	long int hc_getstepZ();

	void hc_setXmotor(long int step);
	void hc_setYmotor(long int step);
	void hc_setZmotor(long int step);
	void hc_setaction(int step);

	friend vector<hcode> hc_generator(disassembled_gcode pre_gc, disassembled_gcode cur_gc);
	friend vector<hcode>  hc_RapidPotioning(float* prev, float* cur);
	friend vector<hcode>  hc_LinearInterpolation(float* prev, float* cur);
	friend void join_vector(vector<hcode> &src, vector<hcode> des);

	hcode hc_string_to_hcode(string input);
	int  hc_string_to_numb(string input);

	string hcode_to_string();
	string numb_to_string(long int);
	friend vector<hcode> hc_compact(vector<hcode>);

	friend bool hc_equal(hcode a, hcode b);
	int hc_count_element(hcode* src);
	hcode(long _ID,long int x, long int y, long int z, int act);
	hcode();
	~hcode();
};

vector<hcode> hc_generator(disassembled_gcode pre_gc, disassembled_gcode cur_gc);
vector<hcode>  hc_RapidPotioning(float* prev, float* cur);
vector<hcode>  hc_LinearInterpolation(float* prev, float* cur);
void join_vector(vector<hcode> &src, vector<hcode> des);
vector<hcode> hc_compact(vector<hcode>);

bool hc_equal(hcode a, hcode b);
