#pragma once
#ifndef MAIN_H
#define MAIN_H

/*LIBRARY*/
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
/*USER LIBRARY*/
#include "gcode_handling.h"
#include "hcode_handling.h"

/*CHOOSE PLATFORM*/
#define WINDOWS

#ifndef WINDOWS
	#define LINUX
#endif

/*DEFINE*/
#define SDCARD

#define CMD_GET_Z_LEVELING		 "G90 Z0.0000"
#define CMD_CHANGE_TOOL_MILL		 "G77 Z0.0000"
#define CMD_CHANGE_TOOL_SIDE		 "G83 Z0.0000"
#define CMD_CHANGE_TOOL_DRILL		 "G68 Z0.0000"
#define CMD_CHANGE_TOOL_CUT		 "G67 Z0.0000"
#define CMD_ZEROS_POSITION		 "G01 X0.0000Y0.0000"
#define CMD_IN_THE_END			 "G69 Z0.0000"
#define CMD_AUTO_Z_ID			 (int)65
#define CMD_NO_STEP_ID			 (int)72
#define CMD_TURN_ON_SPINDLE_LASER	 (int)3
#define CMD_TURN_OFF_SPINDLE_LASER  (int)5

#define LENGTH_PER_HZ (double)0.00125 // 3200Hz --> 1.25mm
long int ID_counter = 2;
/**/
enum file
{
	one_side = 0,
	two_side,
	drill,
	board_cutter
};
enum descartes_size
{
    min_x= 0,
    min_y,
    max_x,
    max_y,
};
/*H�m*/
void menu(int* result);
void generate_gcode_file(int *chosen);
void gc_size_calculator(float *prev, disassembled_gcode cur);
#endif
