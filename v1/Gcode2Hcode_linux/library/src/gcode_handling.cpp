#include "gcode_handling.h"


disassembled_gcode disassembled_gcode::gc_disassembled(string input)
{
	int j = 4, k;
	bool	X_negative_flag = false,
			Z_negative_flag = false,
			Y_negative_flag = false;
	
	command_type = input[0];
	
	action = (gcode_action)((input[G_VALUE_POSITION] - '0')*10 + (input[G_VALUE_POSITION + 1] - '0'));
	
	if (command_type == 'G')
	{
	    if (input[Z_VALUE_POSITION] == 'Z')
		   Z_AXIS_NULL = false;
	    else
		   Z_AXIS_NULL = true;

	    while (input[j] != '\0')
	    {
		   k = j;
		   j++;
		   if (input[k] >= 'A' && input[k] <= 'Z')
		   {
			  while (1)
			  {
				 if (input[j] >= '0' && input[j] <= '9')
					variable[(gcode_variable)input[k] - 65] = (float)(variable[(gcode_variable)input[k] - 65] * 10 + (input[j] - 48));
				 else if (input[j] == '-')
				 {
					switch (input[k])
					{
					case 'X':	X_negative_flag = true;
					    break;
					case 'Y':	Y_negative_flag = true;
					    break;
					case 'Z':	Z_negative_flag = true;
					    break;
					}
				 }
				 else if (input[j] != '.')
				 {
					k = j;
					break;
				 }
				 j++;
			  }

		   }

	    }

	    variable[X_VALUE_GCODE] /= 10000;
	    variable[Y_VALUE_GCODE] /= 10000;
	    variable[Z_VALUE_GCODE] /= 10000;

	    variable[X_VALUE_GCODE] *= (X_negative_flag) ? -1 : 1;
	    variable[Y_VALUE_GCODE] *= (Y_negative_flag) ? -1 : 1;
	    variable[Z_VALUE_GCODE] *= (Z_negative_flag) ? -1 : 1;
	}
	return *this;
}
gcode_action disassembled_gcode::gc_get_disassembled_action()
{
	return action;
}
float* disassembled_gcode::gc_get_disassembled_variable()
{
	return variable;
}

char disassembled_gcode::gc_get_command_type()
{
    return command_type;
}

bool disassembled_gcode::gc_ZAxisIsNull()
{
	return Z_AXIS_NULL ? true : false;
}




disassembled_gcode::disassembled_gcode(gcode_action action)
{
	this->action = action;
}

disassembled_gcode::disassembled_gcode(float x_potion, float y_potion, float z_potion)
{
	variable[X_VALUE_GCODE] = x_potion;
	variable[Y_VALUE_GCODE] = y_potion;
	variable[Z_VALUE_GCODE] = z_potion;
}

disassembled_gcode::disassembled_gcode()
{
	for (int i = 0; i < 26; i++)
		variable[i] = 0;
	action = stop;
}


disassembled_gcode::~disassembled_gcode()
{
}
