#pragma once
#include <string>
#include <iostream>
using namespace std;
/*DEFINE*/
#define G_VALUE_POSITION	1
#define X_VALUE_POSITION
#define Z_VALUE_POSITION	4//use to check Z_AXIS_NULL

#define XY_AXIS_NOT_CHANGE 1
#define Z_AXIS_NOT_CHANGE -1
/*CODE*/

enum gcode_action
{
    normal		    = 0,
    fastmove		    = 1,
    stop			    = -1,
    change_tool_drill   = 68,
    change_tool_milling = 77,
    change_tool_cut	    = 67,
    change_side	    = 83,
    end			    = 69
};

enum gcode_variable
{
	F_VALUE_GCODE = 5,
	G_VALUE_GCODE = 6,
	M_VALUE_GCODE = 12,
	P_VALUE_GCODE = 15,
	X_VALUE_GCODE = 23,
	Y_VALUE_GCODE = 24,
	Z_VALUE_GCODE = 25
};

class disassembled_gcode
{
    char command_type;
    gcode_action action;
    float variable[26];
    bool Z_AXIS_NULL;
public:
	disassembled_gcode gc_disassembled(string);
	gcode_action gc_get_disassembled_action();
	float* gc_get_disassembled_variable();
	char	  gc_get_command_type();
	bool gc_XYAxisIsNull();
	bool gc_ZAxisIsNull();
	int gc_FindNullAxis(disassembled_gcode previous_gcode);
	
	disassembled_gcode(gcode_action action);
	disassembled_gcode(float x_potion, float y_potion, float z_potion);
	disassembled_gcode();
	~disassembled_gcode();
};

