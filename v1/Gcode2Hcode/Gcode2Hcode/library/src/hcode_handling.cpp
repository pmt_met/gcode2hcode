#include "hcode_handling.h"
#include <stdlib.h>




 extern long int ID_counter;

void hcode::hc_setstepX(long int step)
{
	stepX = step;
}
void hcode::hc_setstepY(long int step)
{
	stepY = step;
}
void hcode::hc_setstepZ(long int step)
{
	stepZ = step;
}


long int hcode::hc_getstepX()
{
	return stepX;
}
long int hcode::hc_getstepY()
{
	return stepY;
}
long int hcode::hc_getstepZ()
{
	return stepZ;
}

void hcode::hc_setXmotor(long int step)
{
	stepX = step;
}
void hcode::hc_setYmotor(long int step)
{
	stepY = step;
}
void hcode::hc_setZmotor(long int step)
{
	stepZ = step;
}
void hcode::hc_setaction(int fm)
{
	action = fm;
}
/*
1Hz--------------0.0008mm

*/
vector<hcode> hc_generator(disassembled_gcode pre_gc, disassembled_gcode cur_gc)
{
	vector<hcode> result;

	float *pre = pre_gc.gc_get_disassembled_variable();
	float *cur = cur_gc.gc_get_disassembled_variable();

	join_vector(result, hc_RapidPotioning(pre, cur));

	result[0].action = cur_gc.gc_get_disassembled_action();
	return result;
}
hcode hcode::zzzz(int x1, int y1, int &x2, int &y2)
{
    stepX = x1 - x2;
    stepY = y1 - y2;

    ID = ID_counter;
    ID_counter++;
    x2 = x1;
    y2 = y1;

    return *this;
}

#if defined(BRESENHAM)
vector<hcode> hc_RapidPotioning(float* prev, float* cur) // previous/current variable
{
   
	vector<hcode> result;
	hcode temp;
	int dx, dy, x, y;
	int x_unit = 1, y_unit = 1;

	int x1 = x = (float)prev[X_VALUE_GCODE] / LENGTH_PER_HZ;
	int y1 = y = (float)prev[Y_VALUE_GCODE] / LENGTH_PER_HZ;
	int z1 = (float)prev[Z_VALUE_GCODE] / LENGTH_PER_HZ;

	int x2 = (float)cur[X_VALUE_GCODE] / LENGTH_PER_HZ;
	int y2 = (float)cur[Y_VALUE_GCODE] / LENGTH_PER_HZ;
	int z2 = (float)cur[Z_VALUE_GCODE] / LENGTH_PER_HZ;

	dx = (x2 - x1);			dy = (y2 - y1);

	

	if (z2 - z1)
	{
		temp.stepZ = z2 - z1;
		
		temp.ID = ID_counter;
		result.push_back(temp);
		ID_counter++;
	}
	else if (x1 == x2)   // duong thang dung
	{
		temp.stepX = 0;
		temp.stepY = y2 - y1;
		
		temp.ID = ID_counter;
		result.push_back(temp);
		ID_counter++;
	}
	else if (y1 == y2)  // duong ngang
	{
		temp.stepX = x2 - x1;
		temp.stepY = 0;
		
		temp.ID = ID_counter;
		result.push_back(temp);
		ID_counter++;
	}
	else if (abs(dx) == abs(dy))
	{
		temp.stepY = dy;
		temp.stepX = dx;
		
		temp.ID = ID_counter;
		result.push_back(temp);
		ID_counter++;
	}
	else //if (x1 != x2 && y1 != y2)  // duong xien
	{
		/*prepair step*/
		if (dy < 0)
		{
			dy = -dy;
			y_unit = -1;
		}
		else
			y_unit = 1;

		if (dx < 0)
		{
			dx = -dx;
			x_unit = -1;
		}
		else
			x_unit = 1;

		dy <<= 1;        // dy is now 2*dy
		dx <<= 1;        // dx is now 2*dx

		/*Bressenham*/
		if (dx > dy)
		{
			int fraction = dy - (dx >> 1);  // same as 2*dy - dx
			while (x != x2)
			{
				if (fraction >= 0)
				{
					y += y_unit;
					fraction -= dx;          // same as fraction -= 2*dx
				}
				x += x_unit;
				fraction += dy;              // same as fraction -= 2*dy

				temp.stepX = x - x1;
				temp.stepY = y - y1;
				
				
				temp.ID = ID_counter;
				result.push_back(temp);
				ID_counter++;
				
 				x1 = x;
				y1 = y;
			}
		}
		else
		{
			int fraction = dx - (dy >> 1);
			while (y != y2)
			{
				if (fraction >= 0)
				{
					x += x_unit;
					fraction -= dy;
				}
				y += y_unit;
				fraction += dx;

				temp.stepX = x - x1;
				temp.stepY = y - y1;
				
				temp.ID = ID_counter;
				result.push_back(temp);
				ID_counter++;

				x1 = x;
				y1 = y;
			}
		}
		
	}
	return result;
}
#elif defined(DDA)
vector<hcode> hc_RapidPotioning(float* prev, float* cur) // previous/current variable
{

    vector<hcode> result;
    hcode temp;
    int dx, dy, x, y;
    int x_unit = 1, y_unit = 1;

    int x1 = x = (float)prev[X_VALUE_GCODE] / LENGTH_PER_HZ;
    int y1 = y = (float)prev[Y_VALUE_GCODE] / LENGTH_PER_HZ;
    int z1 = (float)prev[Z_VALUE_GCODE] / LENGTH_PER_HZ;

    int x2 = (float)cur[X_VALUE_GCODE] / LENGTH_PER_HZ;
    int y2 = (float)cur[Y_VALUE_GCODE] / LENGTH_PER_HZ;
    int z2 = (float)cur[Z_VALUE_GCODE] / LENGTH_PER_HZ;

    dx = (x2 - x1);			dy = (y2 - y1);



    if (z2 - z1)
    {
	   temp.stepZ = z2 - z1;

	   temp.ID = ID_counter;
	   result.push_back(temp);
	   ID_counter++;
    }
    else if (x1 == x2)   // duong thang dung
    {
	   temp.stepX = 0;
	   temp.stepY = y2 - y1;

	   temp.ID = ID_counter;
	   result.push_back(temp);
	   ID_counter++;
    }
    else if (y1 == y2)  // duong ngang
    {
	   temp.stepX = x2 - x1;
	   temp.stepY = 0;

	   temp.ID = ID_counter;
	   result.push_back(temp);
	   ID_counter++;
    }
    else if (abs(dx) == abs(dy))
    {
	   temp.stepY = dy;
	   temp.stepX = dx;

	   temp.ID = ID_counter;
	   result.push_back(temp);
	   ID_counter++;
    }
    else //if (x1 != x2 && y1 != y2)  // duong xien
    {
	   /**/

	   if (x2 >= x1)
	   {
		  dx = x2 - x1;
		  x_unit = 1;
	   }
	   else
	   {
		  dx = x1 - x2;
		  x_unit = -1;
	   }

	   if (y2 >= y1)
	   {
		  dy = y2 - y1;
		  y_unit = 1;
	   }
	   else
	   {
		  dy = y1 - y2;
		  y_unit = -1;
	   }

	   int balance;

	   if (dx > dy)
	   {
		  dy <<= 1;
		  balance = dy - dx;
		  dx <<= 1;

		  while (x != x2)
		  {

			 temp.zzzz( x, y, x1, y1);
			 result.push_back(temp);
			 if (balance >= 0)
			 {
				y += y_unit;
				balance -= dx;
			 }
			 balance += dy;
			 x += x_unit;
		  }
		  temp.zzzz(x, y, x1, y1);
		  result.push_back(temp);
	   }
	   else
	   {
		  dx <<= 1;
		  balance = dx - dy;
		  dy <<= 1;

		  while (y != y2)
		  {
			 temp.zzzz(x, y, x1, y1);
			 result.push_back(temp);
			 if (balance >= 0)
			 {
				x += x_unit;
				balance -= dy;
			 }
			 balance += dx;
			 y += y_unit;
		  }
		  temp.zzzz(x, y, x1, y1);
		  result.push_back(temp);
	   }
	   /**/
	   /*prepair step*/


    }
    return result;
}


#endif

vector<hcode> hc_LinearInterpolation(float* prev, float* cur)
{
	vector<hcode> temp;
	return temp;
}

void join_vector(vector<hcode> &src, vector<hcode> des)
{
	int no_elementDes = des.size();
	for (int i = 0; i < no_elementDes; i++)
	{
		src.push_back(des[i]);
	}
}
hcode hcode::hc_string_to_hcode(string input)
{

	return hcode();
}
int hcode::hc_string_to_numb(string input)
{
	int i = 0, sign = 1;
	int result = 0;
	while (input[i])
	{
		if (input[i] == '-')
			sign = -1;
		else
			result = result * 10 + input[i] - 48;
		i++;
	}
	return sign*result;
}
string hcode::hcode_to_string()
{
	char result[50];
	char temp[20];

	_PRINTF(temp, "%d", ID);
	_STRCPY(result, temp);
	
	_STRCAT(result, " ");

	_PRINTF(temp, "%d", action);

	if (action < 10)
	{
	    _STRCAT(result, "0");
	    _STRCAT(result, temp);
	}
	else
	{
	    _STRCAT(result, temp);
	}
	_STRCAT(result, "X");
	_PRINTF(temp, "%d", stepX);
	_STRCAT(result, temp);

	_STRCAT(result, "Y");
	_PRINTF(temp, "%d", stepY);
	_STRCAT(result, temp);

	_STRCAT(result, "Z");
	_PRINTF(temp, "%d", stepZ);
	_STRCAT(result, temp);

	return result;
}

vector<hcode> hc_compact(vector<hcode> h_src)
{
	vector<hcode> result;
	hcode h_cur, h_next, temp;
	bool hc_equal_flag = false;
	unsigned int i = 0, j = 1;
	while (i < h_src.size())
	{
		temp = h_src[i];
		if (j >= h_src.size())
		{
			result.push_back(temp);
			break;
		}
		//case change tool
		if (temp.stepX == 0 && temp.stepY == 0 && temp.stepZ == 0 && 
		    temp.action != normal && temp.action != fastmove)
		{
			result.push_back(temp);
		}
		//case not move
		else if (!(temp.stepX == 0 && temp.stepY == 0 && temp.stepZ == 0))
		{
			while (hc_equal(h_src[i], h_src[j]))
			{
				temp.stepX += h_src[j].stepX;
				temp.stepY += h_src[j].stepY;
				temp.stepZ += h_src[j].stepZ;
				j++;
				if (j >= h_src.size())
					break;
			}
			result.push_back(temp);
		}
		/*else
			cout << "\nzz";*/
		
		
		i = j;
		j = i + 1;
	}
	return result;
}

bool hc_equal(hcode a, hcode b)
{

	if(a.stepX == b.stepX && a.stepY == b.stepY && a.stepZ == b.stepZ)
		return true;
	return false;
}

hcode::hcode(long _ID, long int x, long int y, long int z, int act)
{
    ID = _ID;
    stepX = x;
    stepY = y;
    stepZ = z;
    action = act;
}
hcode::hcode()
{
	stepX = stepY = stepZ = 0;
	action = 0;
	ID = 0;
}

hcode::~hcode()
{
}