#include "main.h"

#define VALID_STRING_CHECKER(a) (a != "" && a != "G21" && a != "G90" && a != "G4 P1" && a != "G94" && a != "G00 X0Y0" && a != "F76.20") 
extern long int ID_counter;

//#define ALGIROTHM_TESTER
#if defined(ALGIROTHM_TESTER)

#define SURFACE int
void myPixel(SURFACE* surface, int x, int y);
// Bresenham Line Algorithm
void myLine(SURFACE* surface, int x1, int y1, int x2, int y2);

ofstream file_hcode;
int main()
{
    float a[26], b[26];
    a['x' - 'a'] = 0;
    a['y' - 'a'] = 0;

    b['x' - 'a'] = 10;
    b['y' - 'a'] = 5;
    hc_RapidPotioning(a, b);

    return 0;


    void myPixel(SURFACE* surface, int x, int y)
    {

	   // PLOT x,y point on surface
	   file_hcode << endl << x << "," << y;
    }


    // Bresenham Line Algorithm
    void myLine(SURFACE* surface, int x1, int y1, int x2, int y2)
    {
	   int		x, y;
	   int		dx, dy;
	   int		incx, incy;
	   int		balance;



	   if (x2 >= x1)
	   {
		  dx = x2 - x1;
		  incx = 1;
	   }
	   else
	   {
		  dx = x1 - x2;
		  incx = -1;
	   }

	   if (y2 >= y1)
	   {
		  dy = y2 - y1;
		  incy = 1;
	   }
	   else
	   {
		  dy = y1 - y2;
		  incy = -1;
	   }

	   x = x1;
	   y = y1;

	   if (dx >= dy)
	   {
		  dy <<= 1;
		  balance = dy - dx;
		  dx <<= 1;

		  while (x != x2)
		  {
			 myPixel(surface, x, y);
			 if (balance >= 0)
			 {
				y += incy;
				balance -= dx;
			 }
			 balance += dy;
			 x += incx;
		  } myPixel(surface, x, y);
	   }
	   else
	   {
		  dx <<= 1;
		  balance = dx - dy;
		  dy <<= 1;

		  while (y != y2)
		  {
			 myPixel(surface, x, y);
			 if (balance >= 0)
			 {
				x += incx;
				balance -= dy;
			 }
			 balance += dx;
			 y += incy;
		  } myPixel(surface, x, y);
	   }
    }

}
#else
int main()
{
    /*delare variable */
    string gcode_str;
    unsigned long int i = 0;


    ifstream file_gcode;
    ofstream file_hcode;

    vector<disassembled_gcode> gcode_vector;
    vector<hcode> hc_vector_result;
    hcode hc_temp;
    disassembled_gcode temp_gc;

    disassembled_gcode present_potion_xy = disassembled_gcode(0, 0, 0);
    disassembled_gcode present_potion_z = disassembled_gcode(0, 0, 0);

    /**/
    int chosen[4] = { 0,0,0,0 };
    float size_of_file[4] = { 0,0,0,0 };
    menu(chosen);
    generate_gcode_file(chosen);

    file_gcode.open("./file/gcode.g");
    file_hcode.open("./file/hcode.hc");

    /*Hcode processing*/
    cout << "\nTransfering...";

    while (!file_gcode.eof())
    {

	   /*Decode gcode string*/
	   getline(file_gcode, gcode_str); //doc gcode
								/*blank line*/
	   if (VALID_STRING_CHECKER(gcode_str))
	   {
		  gcode_vector.push_back(temp_gc.gc_disassembled(gcode_str)); // decode just received string and push to gcode vector manager 
		  gc_size_calculator(size_of_file, temp_gc);

		  /*Tranform to hcode*/
		  //if present command is XY moving command
		  if (gcode_vector[i].gc_get_command_type() == 'M')
		  {
			 hc_temp = hcode(ID_counter, 0, 0, 0, gcode_vector[i].gc_get_disassembled_action());
			 ID_counter++;
			 hc_vector_result.push_back(hc_temp);
		  }
		  else if (gcode_vector[i].gc_get_command_type() == 'G')
		  {
			 if (gcode_vector[i].gc_ZAxisIsNull())
			 {
				join_vector(hc_vector_result, hc_generator(present_potion_xy, gcode_vector[i]));
				present_potion_xy = gcode_vector[i]; //update present potion
			 }
			 //if present command is Z moving command
			 else if (!gcode_vector[i].gc_ZAxisIsNull())
			 {
				join_vector(hc_vector_result, hc_generator(present_potion_z, gcode_vector[i]));
				present_potion_z = gcode_vector[i]; //update present potion
			 }
			 //reset value of temp_gc to default
			 temp_gc = disassembled_gcode();

		  }
		  i++;
	   }
    }
    cout << "..done!!!";
    /*------------------------------------------------------------------*/
#define ROUND(a)				 (int) ((float)a/10 + 1)

    cout << "\nWriting to hcode file";
    /*Wrie size of board, that use to get martrix auto z leveling*/
    hcode a;
    int row = ROUND(size_of_file[max_x]);
    int col = ROUND(size_of_file[max_y]);

    a = hcode(0, row, col, 0, CMD_AUTO_Z_ID);
    file_hcode << a.hcode_to_string() << "." << endl;
    /*Wrie number of hcode*/
    long Number_of_hcode_command = hc_vector_result.size() - 1;

    a = hcode(1, Number_of_hcode_command, 0, 0, CMD_NO_STEP_ID);
    file_hcode << a.hcode_to_string() << "." << endl;
    /**/
    hc_vector_result = hc_compact(hc_vector_result); // rut gon
    for (i = 0; i < hc_vector_result.size(); i++)
    {
#ifndef SDCARD
	   file_hcode << "\nhcode_array[" << i << "].hcode_string = \"";
	   file_hcode << hc_vector_result[i].hcode_to_string();
	   file_hcode << ".\"\;";
#else
	   file_hcode << hc_vector_result[i].hcode_to_string() << "." << endl;
#endif

    }
    cout << "..done!!!";


    file_gcode.close();
    file_hcode.close();
    cout << "\nDone :D" << endl;

    getchar();
    return 0;
}
#endif /*ALGIROTHM_TESTER*/
void menu(int* result)
{
    char chosen = 0x00;
    cout << "\nTop side<<";
    cin >> chosen;
    result[two_side] = (int)(chosen - '0');
    if (result[two_side] == 0)
    {
	   cout << "\nBot side<<";
	   cin >> chosen;
	   result[one_side] = (int)(chosen - '0');;
    }

    cout << "\nDrill<<";
    cin >> chosen;
    result[drill] = (int)(chosen - '0');;


    cout << "\nCut board<<";
    cin >> chosen;
    result[board_cutter] = (int)(chosen - '0');;
}

void generate_gcode_file(int *chosen)
{
    cout << "\nGenneration Gcode file";
    ifstream file_bot_side, file_top_side, file_drill, file_cut;
    ofstream  file_gcode;

    /**/
    file_bot_side.open("./file/bot.g");
    file_top_side.open("./file/top.g");
    file_drill.open("./file/drill.g");
    file_cut.open("./file/cut.g");

    /**/

    file_gcode.open("./file/gcode.g");// , ios::out | ios::in | ios::trunc);


    if (chosen[one_side])
    {
	   file_gcode << CMD_CHANGE_TOOL_MILL << endl << CMD_GET_Z_LEVELING << endl << file_bot_side.rdbuf() << endl << CMD_ZEROS_POSITION;
    }
    /*-------------------------------------------------------------*/
    if (chosen[drill])
    {
	   if (chosen[one_side])
		  file_gcode << endl;

	   file_gcode << CMD_CHANGE_TOOL_DRILL << endl << CMD_GET_Z_LEVELING << endl << file_drill.rdbuf() << endl << CMD_ZEROS_POSITION << endl;

	   //file_gcode << CMD_CHANGE_TOOL_SIDE << endl << CMD_CHANGE_TOOL_MILL << endl << CMD_GET_Z_LEVELING << endl << file_top_side.rdbuf() << endl << CMD_ZEROS_POSITION;
    }
    if (chosen[two_side])
    {
	   if (chosen[drill] || chosen[one_side])
		  file_gcode << endl;

	   file_gcode << CMD_CHANGE_TOOL_MILL << endl << CMD_GET_Z_LEVELING << endl << file_top_side.rdbuf() << endl << CMD_ZEROS_POSITION;
    }
    /*-------------------------------------------------------------*/
    if (chosen[board_cutter])
    {
	   if (chosen[drill] || chosen[two_side] || chosen[one_side])
		  file_gcode << endl;

	   file_gcode << CMD_CHANGE_TOOL_CUT << endl << CMD_GET_Z_LEVELING << endl << file_cut.rdbuf() << endl << CMD_ZEROS_POSITION;
    }
    file_gcode << endl << CMD_IN_THE_END;

    cout << endl << "..DONE!!!";
    /*Empty cache, avoid cache conflict*/
    file_gcode.flush();

    /*Close file*/
    file_bot_side.close();
    file_top_side.close();
    file_drill.close();
    file_cut.close();
    file_gcode.close();
}

void gc_size_calculator(float *prev, disassembled_gcode cur)
{

    float cur_x = cur.gc_get_disassembled_variable()[X_VALUE_GCODE];
    float cur_y = cur.gc_get_disassembled_variable()[Y_VALUE_GCODE];

    if (prev[min_y] > cur_x)
	   prev[min_y] = cur_x;

    if (prev[min_x] > cur_x)
	   prev[min_x] = cur_x;

    if (prev[max_x] < cur_x)
	   prev[max_x] = cur_x;

    if (prev[max_y] < cur_y)
	   prev[max_y] = cur_y;

}

/**/

/**/
